/**
 * Copyright (c) 2017, Vironit and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * This code is part of VironIT software; Part of Relief app.
 *
 * Node example app -  part of main app 
 *
 * @author   VironIT
 * @version  12.1
 * @since    1.0
 * 
 */  

/**
 * define variables cryptoLibrary, fs, connections, assets
 */
var cryptoLibrary = require(__base + 'cryptoLibrary.js');
var fs = require('fs');
var connections = require(__base + 'models/connection');
var assets = require(__base + 'models/assets');

/**
 * 
 * @param  data
 * @param  cb 
 * 
 * @return cb
 */
var createCrossPromotion = function (data, cb) {
    var connection = null;
    connections.getConnection(function (err, connectionT) {
        if (err) return cb(500, err);
        connection = connectionT;
        beginTransaction();
    });

    /**
     * [sendError description]
     * @param  status - error status
     * @param err 
     * @return cb 
     */
    function sendError(status, err) {
        connection.rollback(function () {
            connection.release();
            return cb(status, err);
        });
    }

    function sendResult(data) {
        connection.commit(function (err) {
            if (err) console.log('Commit transaction=', err);
            connection.release();
            return cb(200, data);
        });
    }

    /**
     * Create transaction for new crossPromotion, because we have variable crossPromotion data.
     * Insert into BD const data for crossPromotion.
     * 
     */
    function beginTransaction() {
        connection.beginTransaction(function (err) {
            if (err) {
                console.log('Begin transaction=', err);
                return sendError(500, err);
            }
            var dataAttributesArray = [];
            dataAttributesArray.push(data.crossPromotionTableId);
            dataAttributesArray.push(data.index);
            dataAttributesArray.push(data.order);
            dataAttributesArray.push(data.showInPopup);
            dataAttributesArray.push(data.showInTable);
			// Insert into BD const data for crossPromotion
            connection.query("INSERT INTO CrossPromotions (`crossPromotionTableId`, `index`, `order`, `showInPopup`, " +
                "`showInTable`) VALUES (?, ?, ?, ?, ?)", dataAttributesArray, function (err, insertCrossPromotionData) {
                if (err) return sendError(500, err);
                if (insertCrossPromotionData.affectedRows < 1 || !insertCrossPromotionData.insertId) return sendError(500, 'SQL Insert operation error');
                insertVariableCrossPromotions(insertCrossPromotionData.insertId);
            });
        });
    }

    /**
     * Insert into BD variable data for crossPromotion
     * @param  {int} crossPromotionId 
     * @return 
     */
    function insertVariableCrossPromotions(crossPromotionId) {
        var dataAttributesArray = [];
        var test = data.languages.reduce(function (result, languageData) {
            dataAttributesArray.push(crossPromotionId);
            dataAttributesArray.push(languageData.language);
            dataAttributesArray.push(languageData.title);
            dataAttributesArray.push(languageData.description ? languageData.description : null);
            return result += '(?, ?, ?, ?), ';
        }, '');
		// Insert into BD variable data for crossPromotion
        connection.query("INSERT INTO VariableCrossPromotions (`crossPromotionId`, `language`, `title`, `description`)" +
            " VALUES " + test.substring(0, test.length - 2), dataAttributesArray, function (err, insertVarCrossPromotionsData) {
            if (err) return sendError(500, {err: err});
            if (insertVarCrossPromotionsData.affectedRows != data.languages.length) return sendError(500, 'SQL Insert operation error');
            createAsset(crossPromotionId);
        });
    }

    /**
     * Create asset for current crossPromotion
     * 
     * @param  {integer} crossPromotionId 
     * 
     * @return sendResult
     */
    function createAsset(crossPromotionId) {
        if (!data.asset) return sendError(400, 'Bad data.');
		// Create asset for current crossPromotion
        assets.create(Object.assign(data.asset, {crossPromotionId: crossPromotionId}), connection, function (err, data) {
            if (err) return sendError(err, data);
            return sendResult({
                id: crossPromotionId
            });
        });
    }
}
/**
 * Search crossPromotion by different const data attributes.
 * 
 * @param    data
 * @param  {Function} cb 
 * @return {Function} cb
 */
module.exports.read = function (data, cb) {
    if (Object.keys(data).length == 0) return cb(400, {err: 'Bad data'});
    __MYSQL_POOL.getConnection(function (err, connection) {
        if (err) return cb(500, err);
        var dataAttributesArray = [];
        if (data.index) dataAttributesArray.push(data.index);
        if (data.crossPromotionTableId) dataAttributesArray.push(data.crossPromotionTableId);
        if (data.id) dataAttributesArray.push(data.id);
		// Search crossPromotion by different const data attributes
        var query = "SELECT `id`, `crossPromotionTableId`, `index`, `order`, `showInPopup`, " +
            "`showInTable` FROM CrossPromotions WHERE " + (data.index ? "`index` = ? AND " : "") +
            (data.crossPromotionTableId ? "`crossPromotionTableId` = ? AND " : "") + (data.id ? "`id` = ? AND " : "");
        connection.query(query.substring(0, query.length - 5), dataAttributesArray, function (err, selectCrossPromotionData) {
            connection.release();
            if (err) return cb(500, {err: err});
            if (selectCrossPromotionData.length == 0) return cb(null, selectCrossPromotionData);
            getVariableCrossPromotions(selectCrossPromotionData);
        });
    });

    /**
     * Get variables. Search crossPromotion by languane for obtain variable data attributes.
     * 
     * @param  selectCrossPromotionData 
     * @return crossPromotion or cb or variables cross promotion
     */
    function getVariableCrossPromotions(selectCrossPromotionData) {
        __MYSQL_POOL.getConnection(function (err, connection) {
            if (err) return cb(500, err);
            var crossPromotionIds = selectCrossPromotionData.map(function (crossPromotion) {
                return crossPromotion.id;
            });
            var array = [];
            array.push(crossPromotionIds);
            data.language ? array.push(data.language) : null;
			// Search crossPromotion by languane for obtain variable data attributes
            connection.query("SELECT `crossPromotionId`, `language`, `title`, `description` FROM VariableCrossPromotions WHERE " +
                "`crossPromotionId` IN (?)" + (data.language ? " AND `language` = ?" : ""), array, function (err, selectVariableCrossPromotionData) {
                connection.release();
                if (err) return cb(500, {err: err});
                if (data.language && selectVariableCrossPromotionData.length < 1) return cb(400, {err: 'Bad language'});
                return cb(null, selectCrossPromotionData.map(function (crossPromotion) {
                    crossPromotion.showInPopup = crossPromotion.showInPopup ? true : false;
                    crossPromotion.showInTable = crossPromotion.showInTable ? true : false;
                    crossPromotion.languages = selectVariableCrossPromotionData.filter(function (variableCrossPromotion) {
                        return variableCrossPromotion.crossPromotionId == crossPromotion.id;
                    }).map(function (variableCrossPromotion) {
                        delete variableCrossPromotion.crossPromotionId;
                        return variableCrossPromotion;
                    });
                    return crossPromotion;
                }));
            });
        });
    }
}

/**
 * Update.
 * Create transaction for update crossPromotion, because we can update variable crossPromotion data.
 * Check input data with old data for changes.
 * If we have changes in const attributes, update them.
 * If we have changes in const attributes, update them.
 * 
 * @param id     
 * @param data   
 * @param oldData 
 * @param  {Function} cb 
 * @return cb
 */
module.exports.update = function (id, data, oldData, cb) {
    var dataFlag = false;
    var languagesFlag = false;

    if (Object.keys(data).length == 0) return cb(400, {err: 'Bad data'});
    __MYSQL_POOL.getConnection(function (err, connection) {
        if (err) return cb(500, err);
		// Create transaction for update crossPromotion, because we can update variable crossPromotion data
        connection.beginTransaction(function (err) {
            if (err) console.log('Begin transaction=', err);
            var variableIdx = 0;

            function sendError(status, err) {
                connection.rollback(function () {
                    connection.release();
                    return cb(status, err);
                });
            }

            function sendResult(data) {
                connection.commit(function (err) {
                    if (err) console.log('Commit transaction=', err);
                    connection.release();
                    return cb(null, data);
                });
            }
			// Check input data with old data for changes
            var actualIndex = null, actualOrder = null, actualShowInPopup = null, actualShowInTable = null;
            if (data.index && oldData.index != data.index) actualIndex = data.index;
            if (data.order && oldData.order != data.order) actualOrder = data.order;
            if (data.showInPopup && (new Boolean(oldData.showInPopup) != data.showInPopup)) actualShowInPopup = data.showInPopup;
            if (data.showInTable && (new Boolean(oldData.showInTable) != data.showInTable)) actualShowInTable = data.showInTable;
            if (actualIndex || actualOrder || actualShowInPopup || actualShowInTable) {
                var query = (actualIndex ? " `index` = ?," : "") + (actualOrder ? " `order` = ?," : "")
                    + (actualShowInPopup ? " `showInPopup` = ?," : "")
                    + (actualShowInTable ? " `showInTable` = ?," : "") + '';
                var array = [];
                actualIndex ? array.push(actualIndex) : null;
                actualOrder ? array.push(actualOrder) : null;
                actualShowInPopup ? array.push(actualShowInPopup) : null;
                actualShowInTable ? array.push(actualShowInTable) : null;
                array.push(id);
				// If we have changes in const attributes, update them
                connection.query("UPDATE CrossPromotions SET" + (query.length > 0 ? query.substring(0, query.length - 1) : '') + " WHERE `id` = ?",
                    array, function (err, updateCrossPromotionData) {
                        if (err) return sendError(500, {err: err});
                        if (updateCrossPromotionData.changedRows != 1) return sendError(500, {err: 'SQL update operation error'});
                        updateLanguages(false);
                    });
            }
            else updateLanguages(true);

            function updateLanguages(flag) {
                dataFlag = flag;
				// Search all (with all languages) variable data for current crossPromotion
                connection.query("SELECT `language`, `title`, `description` FROM VariableCrossPromotions WHERE " +
                    "`crossPromotionId` = ?", [id], function (err, selectVarCrossPromotionsData) {
                    if (err) return sendError(500, {err: err});
                    if (selectVarCrossPromotionsData.length == 0) return sendError(500, {err: 'SQL select operation error'});
                    if (!data.languages || (data.language && data.languages.length == 0)) {
                        languagesFlag = true;
                        return updateAsset(data.asset);
                    }
					// Check input data with old data variable attributes for changes
                    var dataForUpdate = data.languages.map(function (language) {
                        var searchData = selectVarCrossPromotionsData.find(function (varCrossPromotion) {
                            return varCrossPromotion.language == language.language;
                        });
                        return {
                            language: language.language,
                            title: (searchData.title != language.title) ? language.title : undefined,
                            description: (searchData.description != language.description) ? language.description : undefined
                        };
                    }).filter(function (language) {
                        return language.title || language.description;
                    });
                    if (!dataForUpdate || (dataForUpdate && !dataForUpdate.length)) return updateAsset(data.asset);
                    variableIdx = dataForUpdate.length;
                    updateLanguage(dataForUpdate, 0, function (err, result) {
                        if (err) return sendError(err, {err: result});
                        return updateAsset(data.asset);
                    });
                });
            }

            /**
             * Update language.
             * If we have changes in variable attributes (for one language), update them.
             * 
             * @param     languagesData
             * @param     idx          
             * @param  {Function} cb           
             * @return {Function} cb            
             */
            function updateLanguage(languagesData, idx, cb) {
                var array = [];
                var query = (languagesData[idx].title ? '`title` = ?, ' : '') + (languagesData[idx].description ? '`description` = ?, ' : '');
                languagesData[idx].title ? array.push(languagesData[idx].title) : null;
                languagesData[idx].description ? array.push(languagesData[idx].description) : null;
                array.push(id);
                array.push(languagesData[idx].language);
				// If we have changes in variable attributes (for one language), update them
                connection.query("UPDATE VariableCrossPromotions SET " + query.substring(0, query.length - 2) +
                    " WHERE `crossPromotionId` = ? AND `language` = ?", array, function (err, updateVariableCrossPromotionData) {
                    if (err) return cb(500, err);
                    if (updateVariableCrossPromotionData.changedRows != 1) return cb(500, 'SQL update operation error');
                    if (++idx == variableIdx) return cb(null, 'OK');
                    updateLanguage(languagesData, idx, cb);
                });
            }
            /**
             * Update asset
             * 
             * @param   assetData 
             * @return {Function} sendError or sendResult
             */
            function updateAsset(assetData) {
				// Update asset
                if (!assetData && languagesFlag && dataFlag) return sendError(400, 'Bad data.');
                else if (!assetData) return sendResult('OK');
                assets.update(assetData, id, connection, function (err, data) {
                    if (err) return sendError(err, data);
                    return sendResult('OK');
                });
            }
        });
    });
}

/**
 * Delete crossPromotion by id.
 * 
 * @param  data - data for delete from database
 * @param  {Function} cb   
 * @return {Function} cb        
 */
module.exports.delete = function (data, cb) {
    __MYSQL_POOL.getConnection(function (err, connection) {
        if (err) return cb(500, err);
		// Delete crossPromotion by id
        connection.query("DELETE FROM CrossPromotions WHERE `id` = ?",
            [data.id], function (err, deletedCrossPromotionData) {
                connection.release();
                if (err) return cb(500, {err: err});
                if (deletedCrossPromotionData.affectedRows < 1) return cb(500, {err: 'SQL Insert operation error'});
                return cb(null, {id: parseInt(data.id)})
            });
    });
}

module.exports.create = createCrossPromotion;