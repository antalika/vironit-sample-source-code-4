using System;
using System.Reflection;

/// <summary>
/// Reflection-based copier of objects.
/// </summary>
public static class ObjectCopier
{
    /// <summary>
    /// Copies the field from the source object to the target.
    /// </summary>
    /// <param name="source">Instance of the source object.</param>
    /// <param name="target">Instance of the target object.</param>
    /// <param name="name">Name of the field.</param>
    public static void CopyField(object source, object target, string name)
    {
        var sourceType = source.GetType();
        var targetType = target.GetType();

        var sourceField = sourceType.GetField(name);
        var targetField = targetType.GetField(name);

        var value = sourceField.GetValue(source);
        targetField.SetValue(target, value);
    }

    /// <summary>
    /// Trying to copy the field from the source object to the target.
    /// </summary>
    /// <param name="source">Instance of the source object.</param>
    /// <param name="target">Instance of the target object.</param>
    /// <param name="name">Name of the field.</param>
    public static bool TryCopyField(object source, object target, string name)
    {
        try { CopyField(source, target, name); }
        catch (Exception) { return false; }

        return true;
    }


    /// <summary>
    /// Copies the property from the source object to the target.
    /// </summary>
    /// <param name="source">Instance of the source object.</param>
    /// <param name="target">Instance of the target object.</param>
    /// <param name="name">Name of the property.</param>
    public static void CopyProperty(object source, object target, string name)
    {
        var sourceType = source.GetType();
        var targetType = target.GetType();

        var sourceProperty = sourceType.GetProperty(name);
        var targetProperty = targetType.GetProperty(name);

        var value = sourceProperty.GetValue(source, null);
        targetProperty.SetValue(target, value, null);
    }

    /// <summary>
    /// Trying to copy the property from the source object to the target.
    /// </summary>
    /// <param name="source">Instance of the source object.</param>
    /// <param name="target">Instance of the target object.</param>
    /// <param name="name">Name of the property.</param>
    public static bool TryCopyProperty(object source, object target, string name)
    {
        try { CopyProperty(source, target, name); }
        catch (Exception) { return false; }

        return true;
    }


    /// <summary>
    /// Clones instance of the object.
    /// </summary>
    /// <returns>Instance of the cloned object.</returns>
    /// <param name="source">Instance of the source object.</param>
    public static object Clone(object source)
    {
        var clonable = source as ICloneable;
        if (clonable != null)
            return clonable.Clone();

        var type = source.GetType();

        var copy = Activator.CreateInstance(type);

        var fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField);
        foreach (var field in fields)
            TryCopyField(source, copy, field.Name);

        var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty | BindingFlags.SetProperty);
        foreach (var property in properties)
            TryCopyProperty(source, copy, property.Name);

        return copy;
    }
}